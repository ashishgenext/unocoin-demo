package com.unocoin.demo.model;

public class ProfileStatusItem{
	private String type;
	private int status;

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ProfileStatusItem{" + 
			"type = '" + type + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}
