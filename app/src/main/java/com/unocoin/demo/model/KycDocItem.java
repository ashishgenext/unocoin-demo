package com.unocoin.demo.model;

import java.util.List;

public class KycDocItem{
	private int kycDocTypeId;
	private String documentNumber;
	private int userId;
	private List<KycDocFileItem> kycDocFile;
	private boolean fileUploaded;
	private String nameInDoc;
	private String dobAsPerDoc;
	private int id;
	private DocType docType;
	private String documentType;
	private int status;

	public void setKycDocTypeId(int kycDocTypeId){
		this.kycDocTypeId = kycDocTypeId;
	}

	public int getKycDocTypeId(){
		return kycDocTypeId;
	}

	public void setDocumentNumber(String documentNumber){
		this.documentNumber = documentNumber;
	}

	public String getDocumentNumber(){
		return documentNumber;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setKycDocFile(List<KycDocFileItem> kycDocFile){
		this.kycDocFile = kycDocFile;
	}

	public List<KycDocFileItem> getKycDocFile(){
		return kycDocFile;
	}

	public void setFileUploaded(boolean fileUploaded){
		this.fileUploaded = fileUploaded;
	}

	public boolean isFileUploaded(){
		return fileUploaded;
	}

	public void setNameInDoc(String nameInDoc){
		this.nameInDoc = nameInDoc;
	}

	public String getNameInDoc(){
		return nameInDoc;
	}

	public void setDobAsPerDoc(String dobAsPerDoc){
		this.dobAsPerDoc = dobAsPerDoc;
	}

	public String getDobAsPerDoc(){
		return dobAsPerDoc;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setDocType(DocType docType){
		this.docType = docType;
	}

	public DocType getDocType(){
		return docType;
	}

	public void setDocumentType(String documentType){
		this.documentType = documentType;
	}

	public String getDocumentType(){
		return documentType;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"KycDocItem{" + 
			"kyc_doc_type_id = '" + kycDocTypeId + '\'' + 
			",document_number = '" + documentNumber + '\'' + 
			",user_id = '" + userId + '\'' + 
			",kyc_doc_file = '" + kycDocFile + '\'' + 
			",file_uploaded = '" + fileUploaded + '\'' + 
			",name_in_doc = '" + nameInDoc + '\'' + 
			",dob_as_per_doc = '" + dobAsPerDoc + '\'' + 
			",id = '" + id + '\'' + 
			",doc_type = '" + docType + '\'' + 
			",document_type = '" + documentType + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}