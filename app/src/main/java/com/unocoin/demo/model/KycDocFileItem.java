package com.unocoin.demo.model;

public class KycDocFileItem{
	private int kycDocId;
	private String fileName;
	private int status;

	public void setKycDocId(int kycDocId){
		this.kycDocId = kycDocId;
	}

	public int getKycDocId(){
		return kycDocId;
	}

	public void setFileName(String fileName){
		this.fileName = fileName;
	}

	public String getFileName(){
		return fileName;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"KycDocFileItem{" + 
			"kyc_doc_id = '" + kycDocId + '\'' + 
			",file_name = '" + fileName + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}
