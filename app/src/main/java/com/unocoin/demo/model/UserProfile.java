package com.unocoin.demo.model;

public class UserProfile{
	private String profileImage;
	private Object gender;
	private String profileImgUrl;
	private int userId;
	private Object doa;
	private Object alternateMobileNumber;

	public void setProfileImage(String profileImage){
		this.profileImage = profileImage;
	}

	public String getProfileImage(){
		return profileImage;
	}

	public void setGender(Object gender){
		this.gender = gender;
	}

	public Object getGender(){
		return gender;
	}

	public void setProfileImgUrl(String profileImgUrl){
		this.profileImgUrl = profileImgUrl;
	}

	public String getProfileImgUrl(){
		return profileImgUrl;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setDoa(Object doa){
		this.doa = doa;
	}

	public Object getDoa(){
		return doa;
	}

	public void setAlternateMobileNumber(Object alternateMobileNumber){
		this.alternateMobileNumber = alternateMobileNumber;
	}

	public Object getAlternateMobileNumber(){
		return alternateMobileNumber;
	}

	@Override
 	public String toString(){
		return 
			"UserProfile{" + 
			"profile_image = '" + profileImage + '\'' + 
			",gender = '" + gender + '\'' + 
			",profile_img_url = '" + profileImgUrl + '\'' + 
			",user_id = '" + userId + '\'' + 
			",doa = '" + doa + '\'' + 
			",alternate_mobile_number = '" + alternateMobileNumber + '\'' + 
			"}";
		}
}
