package com.unocoin.demo.model;

public class UserAddress{
	private String country;
	private int userId;
	private String city;
	private String addressLineTwo;
	private String pinCode;
	private String state;
	private String addressLineOne;

	public void setCountry(String country){
		this.country = country;
	}

	public String getCountry(){
		return country;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setAddressLineTwo(String addressLineTwo){
		this.addressLineTwo = addressLineTwo;
	}

	public String getAddressLineTwo(){
		return addressLineTwo;
	}

	public void setPinCode(String pinCode){
		this.pinCode = pinCode;
	}

	public String getPinCode(){
		return pinCode;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	public void setAddressLineOne(String addressLineOne){
		this.addressLineOne = addressLineOne;
	}

	public String getAddressLineOne(){
		return addressLineOne;
	}

	@Override
 	public String toString(){
		return 
			"UserAddress{" + 
			"country = '" + country + '\'' + 
			",user_id = '" + userId + '\'' + 
			",city = '" + city + '\'' + 
			",address_line_two = '" + addressLineTwo + '\'' + 
			",pin_code = '" + pinCode + '\'' + 
			",state = '" + state + '\'' + 
			",address_line_one = '" + addressLineOne + '\'' + 
			"}";
		}
}
