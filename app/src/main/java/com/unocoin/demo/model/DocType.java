package com.unocoin.demo.model;

public class DocType{
	private int id;
	private String docType;

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setDocType(String docType){
		this.docType = docType;
	}

	public String getDocType(){
		return docType;
	}

	@Override
 	public String toString(){
		return 
			"DocType{" + 
			"id = '" + id + '\'' + 
			",doc_type = '" + docType + '\'' + 
			"}";
		}
}
