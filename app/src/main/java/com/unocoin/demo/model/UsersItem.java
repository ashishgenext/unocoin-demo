package com.unocoin.demo.model;

import java.util.List;

public class UsersItem{
	private int emailVerified;
	private int gaEnabled;
	private Object autoSell;
	private UserAddress userAddress;
	private int sbpEnabled;
	private UserProfile userProfile;
	private List<BankDetailItem> bankDetail;
	private int verificationStatus;
	private List<ProfileStatusItem> profileStatus;
	private int securityQuestions;
	private int userType;
	private String imgUrl;
	private Object name;
	private List<KycDocItem> kycDoc;
	private int id;
	private String mobileNumber;
	private List<Object> netkiStatus;
	private String maxBuyLimit;
	private String maxSellLimit;
	private Object email;
	private int status;

	public void setEmailVerified(int emailVerified){
		this.emailVerified = emailVerified;
	}

	public int getEmailVerified(){
		return emailVerified;
	}

	public void setGaEnabled(int gaEnabled){
		this.gaEnabled = gaEnabled;
	}

	public int getGaEnabled(){
		return gaEnabled;
	}

	public void setAutoSell(Object autoSell){
		this.autoSell = autoSell;
	}

	public Object getAutoSell(){
		return autoSell;
	}

	public void setUserAddress(UserAddress userAddress){
		this.userAddress = userAddress;
	}

	public UserAddress getUserAddress(){
		return userAddress;
	}

	public void setSbpEnabled(int sbpEnabled){
		this.sbpEnabled = sbpEnabled;
	}

	public int getSbpEnabled(){
		return sbpEnabled;
	}

	public void setUserProfile(UserProfile userProfile){
		this.userProfile = userProfile;
	}

	public UserProfile getUserProfile(){
		return userProfile;
	}

	public void setBankDetail(List<BankDetailItem> bankDetail){
		this.bankDetail = bankDetail;
	}

	public List<BankDetailItem> getBankDetail(){
		return bankDetail;
	}

	public void setVerificationStatus(int verificationStatus){
		this.verificationStatus = verificationStatus;
	}

	public int getVerificationStatus(){
		return verificationStatus;
	}

	public void setProfileStatus(List<ProfileStatusItem> profileStatus){
		this.profileStatus = profileStatus;
	}

	public List<ProfileStatusItem> getProfileStatus(){
		return profileStatus;
	}

	public void setSecurityQuestions(int securityQuestions){
		this.securityQuestions = securityQuestions;
	}

	public int getSecurityQuestions(){
		return securityQuestions;
	}

	public void setUserType(int userType){
		this.userType = userType;
	}

	public int getUserType(){
		return userType;
	}

	public void setImgUrl(String imgUrl){
		this.imgUrl = imgUrl;
	}

	public String getImgUrl(){
		return imgUrl;
	}

	public void setName(Object name){
		this.name = name;
	}

	public Object getName(){
		return name;
	}

	public void setKycDoc(List<KycDocItem> kycDoc){
		this.kycDoc = kycDoc;
	}

	public List<KycDocItem> getKycDoc(){
		return kycDoc;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setMobileNumber(String mobileNumber){
		this.mobileNumber = mobileNumber;
	}

	public String getMobileNumber(){
		return mobileNumber;
	}

	public void setNetkiStatus(List<Object> netkiStatus){
		this.netkiStatus = netkiStatus;
	}

	public List<Object> getNetkiStatus(){
		return netkiStatus;
	}

	public void setMaxBuyLimit(String maxBuyLimit){
		this.maxBuyLimit = maxBuyLimit;
	}

	public String getMaxBuyLimit(){
		return maxBuyLimit;
	}

	public void setMaxSellLimit(String maxSellLimit){
		this.maxSellLimit = maxSellLimit;
	}

	public String getMaxSellLimit(){
		return maxSellLimit;
	}

	public void setEmail(Object email){
		this.email = email;
	}

	public Object getEmail(){
		return email;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"UsersItem{" + 
			"email_verified = '" + emailVerified + '\'' + 
			",ga_enabled = '" + gaEnabled + '\'' + 
			",auto_sell = '" + autoSell + '\'' + 
			",user_address = '" + userAddress + '\'' + 
			",sbp_enabled = '" + sbpEnabled + '\'' + 
			",user_profile = '" + userProfile + '\'' + 
			",bank_detail = '" + bankDetail + '\'' + 
			",verification_status = '" + verificationStatus + '\'' + 
			",profile_status = '" + profileStatus + '\'' + 
			",security_questions = '" + securityQuestions + '\'' + 
			",user_type = '" + userType + '\'' + 
			",img_url = '" + imgUrl + '\'' + 
			",name = '" + name + '\'' + 
			",kyc_doc = '" + kycDoc + '\'' + 
			",id = '" + id + '\'' + 
			",mobile_number = '" + mobileNumber + '\'' + 
			",netki_status = '" + netkiStatus + '\'' + 
			",max_buy_limit = '" + maxBuyLimit + '\'' + 
			",max_sell_limit = '" + maxSellLimit + '\'' + 
			",email = '" + email + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}