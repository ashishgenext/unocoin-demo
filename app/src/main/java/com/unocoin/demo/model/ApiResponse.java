package com.unocoin.demo.model;

import java.util.List;

public class ApiResponse{
	private List<UsersItem> users;

	public void setUsers(List<UsersItem> users){
		this.users = users;
	}

	public List<UsersItem> getUsers(){
		return users;
	}

	@Override
 	public String toString(){
		return 
			"ApiResponse{" + 
			"users = '" + users + '\'' + 
			"}";
		}
}